FROM microsoft/dotnet:2.2-runtime

RUN apt-get update
RUN apt-get install -y wget rsync libunwind8
RUN wget -q -O azcopy.tar.gz https://aka.ms/downloadazcopy-v10-linux && \
    tar -xf azcopy.tar.gz && \
    mv azcopy_linux_amd64_10.1.2/azcopy /usr/local/bin/
RUN rm azcopy.tar.gz && rm -r azcopy_linux_amd64_10.1.2
COPY pipe /
RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]
