# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.0.0

- major: Latest az copy

## 1.1.1

- patch: Update license to MIT

## 1.1.0

- minor: Updated pipe to use AzCopy 7.3.0

## 1.0.0

- major: Initial release.

